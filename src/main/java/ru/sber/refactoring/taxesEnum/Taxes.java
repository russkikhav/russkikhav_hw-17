package ru.sber.refactoring.taxesEnum;

/**
 * Перечисление всех возможных налогов
 */
public enum Taxes {
    NALOG(0.2),
    NDFL(0.13);

    private final double taxes;

    Taxes(double taxes) {
        this.taxes = taxes;
    }

    public double getTaxes() {
        return taxes;
    }
}
