package ru.sber.refactoring.sendingMessage;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * отправку сообщения вынес в отдельный класс
 */
public class EmailReportSender implements ReportSender{
    String recipients;
    StringBuilder resultingHtml;

    public EmailReportSender(String recipients, StringBuilder resultingHtml) {
        this.recipients = recipients;
        this.resultingHtml = resultingHtml;
    }

    @Override
    public void sendMessage() throws MessagingException {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        // we're going to use google mail to send this message
        mailSender.setHost("mail.google.com");
        // construct the message
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(recipients);
        // setting message text, last parameter 'true' says that it is HTML format
        helper.setText(resultingHtml.toString(), true);
        helper.setSubject("Monthly department salary report");
        // send the message
        mailSender.send(message);
    }
}
