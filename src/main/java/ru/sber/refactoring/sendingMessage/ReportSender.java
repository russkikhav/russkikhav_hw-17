package ru.sber.refactoring.sendingMessage;

import javax.mail.MessagingException;

public interface ReportSender {
    void sendMessage() throws MessagingException;
}
