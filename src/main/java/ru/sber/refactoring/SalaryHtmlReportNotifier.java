package ru.sber.refactoring;

import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;
import ru.sber.refactoring.dateRange.DateRange;
import ru.sber.refactoring.formattedSalary.FormatSalaryAndTaxes;
import ru.sber.refactoring.repositoy.DataBaseConnection;
import ru.sber.refactoring.sendingMessage.EmailReportSender;
import ru.sber.refactoring.sendingMessage.ReportSender;

import javax.mail.MessagingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SalaryHtmlReportNotifier {
    private static final Logger LOGGER = LoggerFactory.getLogger(SalaryHtmlReportNotifier.class);
    private final Connection connection;

    public SalaryHtmlReportNotifier(Connection databaseConnection) {
        this.connection = databaseConnection;
    }

    public void generateAndSendHtmlSalaryReport(String departmentId, DateRange dateRange, String recipients) {
        try {
            DataBaseConnection dataBaseConnection = new DataBaseConnection(connection);
            // execute query and get the results
            ResultSet results = dataBaseConnection.getPrepareStatement(departmentId, dateRange).executeQuery();
            // create a StringBuilder holding a resulting html
            StringBuilder resultingHtml = new StringBuilder();
            resultingHtml.append("<html><body><table><tr><td>Employee</td><td>Salary</td></tr>");
            //format salary and taxes
            FormatSalaryAndTaxes formatSalaryAndTaxes = new FormatSalaryAndTaxes();
            double totals = formatSalaryAndTaxes.getResult(results, resultingHtml);
            resultingHtml.append("<tr><td>Total</td><td>").append(totals).append("</td></tr>");
            resultingHtml.append("</table></body></html>");
            // now when the report is built we need to send it to the recipients list
            ReportSender reportSender = new EmailReportSender(recipients, resultingHtml);
            reportSender.sendMessage();
        } catch (SQLException e) {
            LOGGER.error("SQL Exception" + e);
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
