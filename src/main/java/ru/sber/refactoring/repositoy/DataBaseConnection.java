package ru.sber.refactoring.repositoy;

import ru.sber.refactoring.dateRange.DateRange;

import java.sql.*;

public class DataBaseConnection {
    private final Connection connection;

    public DataBaseConnection(Connection connection) {
        this.connection = connection;
    }

    public PreparedStatement getPrepareStatement(String departmentId, DateRange dateRange){
        int PARAMETER_INDEX_1 = 1;
        int PARAMETER_INDEX_2 = 2;
        int PARAMETER_INDEX_3 = 3;

        String DEPARTMENT_ID = departmentId;
        Date DATE_TO = new java.sql.Date(dateRange.getDateTo().toEpochDay());
        Date DATE_FROM = new java.sql.Date(dateRange.getDateFrom().toEpochDay());

        // prepare statement with sql
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("SELECT emp.id AS emp_id, " +
                    "emp.name AS amp_name, sum(salary) AS salary FROM employee emp LEFT join " +
                    "salary_payments sp ON emp.id = sp.employee_id WHERE" +
                    " emp.department_id = ? AND " +
                    " sp.date >= ? AND sp.date <= ? GROUP BY emp.id, emp.name");
            // inject parameters to sql
            ps.setString(PARAMETER_INDEX_1, DEPARTMENT_ID);
            ps.setDate(PARAMETER_INDEX_2, DATE_FROM);
            ps.setDate(PARAMETER_INDEX_3, DATE_TO);

            ResultSet results = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ps;
    }
}
