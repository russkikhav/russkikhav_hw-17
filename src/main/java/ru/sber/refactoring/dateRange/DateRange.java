package ru.sber.refactoring.dateRange;

import java.time.LocalDate;

/**
 * Так как аргументы всегда передаются вместе, сделал отдельный объект
 */
public class DateRange {
    private LocalDate DateTo;
    private LocalDate DateFrom;

    public DateRange(LocalDate DateTo, LocalDate DateFrom) {
        this.DateTo = DateTo;
        this.DateFrom = DateFrom;
    }

    public LocalDate getDateTo() {
        return DateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        DateTo = dateTo;
    }

    public LocalDate getDateFrom() {
        return DateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        DateFrom = dateFrom;
    }
}
