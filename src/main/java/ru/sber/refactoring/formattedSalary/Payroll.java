package ru.sber.refactoring.formattedSalary;

import ru.sber.refactoring.formattedSalary.salaryType.SalaryType;
import ru.sber.refactoring.taxesEnum.Taxes;

public class Payroll {
    private final double salary;
    SalaryType salaryType;

    public Payroll(double salary, SalaryType salaryType) {
        this.salaryType = salaryType;
        this.salary = salary;
    }

    public double count() {
        if (salaryType == SalaryType.SALARY_LESS_THAN_10_PROCENT_AVERAGE_SALARY) {
            return 0;
        } else if (salaryType == SalaryType.SALARY_LESS_THAN_AVERAGE_SALARY) {
            return salary * Taxes.NDFL.getTaxes();
        } else if (salaryType == SalaryType.SALARY_MORE_THAN_AVERAGE_SALARY) {
            return salary * Taxes.NALOG.getTaxes();
        }
        throw new RuntimeException("не известный salaryType");
    }
}
