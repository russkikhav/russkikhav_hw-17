package ru.sber.refactoring.formattedSalary;

import ru.sber.refactoring.formattedSalary.salaryType.SalaryType;

public class FormattedSalary {
    private final double salary;
    SalaryType salaryType;

    public FormattedSalary(double salary, SalaryType salaryType) {
        this.salary = salary;
        this.salaryType = salaryType;
    }

    public String getFormatted() {
        Payroll payroll = new Payroll(salary, salaryType);
        return salary + " (taxes: " + payroll.count() + ")";
    }
}
