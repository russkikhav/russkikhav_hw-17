package ru.sber.refactoring.formattedSalary.salaryType;

import ru.sber.refactoring.formattedSalary.FormattedSalary;
import ru.sber.refactoring.formattedSalary.Payroll;
import ru.sber.refactoring.taxesEnum.Taxes;

/**
 * Метод getTypeSalary(double salary) возвращает тип зарплаты:
 * где, 0 - SALARY_LESS_THAN_10_PROCENT_AVERAGE_SALARY - это зарплата меньше 10% от AVERAGE_SALARY
 * 1 - SALARY_LESS_THAN_AVERAGE_SALARY - зарплата меньше AVERAGE_SALARY
 * 2 - SALARY_MORE_THAN_AVERAGE_SALARY - зарплата больше AVERAGE_SALARY
 * <p>
 * статический метод getFormattedTaxesSalary
 * в нем я прохожу по всем перечислениям и сравниваю их с переданной зарплатой
 * и вызываю метод getFormat который вернёт строку в формате с подсчитанным налогом
 */

public enum SalaryType {
    SALARY_LESS_THAN_10_PROCENT_AVERAGE_SALARY() {
        @Override
        public String calculateTaxes() {
            FormattedSalary formattedSalary = new FormattedSalary(salary, SALARY_LESS_THAN_10_PROCENT_AVERAGE_SALARY);
            return formattedSalary.getFormatted();
        }
    },
    SALARY_LESS_THAN_AVERAGE_SALARY() {
        @Override
        public String calculateTaxes() {
            FormattedSalary formattedSalary = new FormattedSalary(salary, SALARY_LESS_THAN_AVERAGE_SALARY);
            return formattedSalary.getFormatted();
        }
    },
    SALARY_MORE_THAN_AVERAGE_SALARY() {
        @Override
        public String calculateTaxes() {
            FormattedSalary formattedSalary = new FormattedSalary(salary, SALARY_MORE_THAN_AVERAGE_SALARY);
            return formattedSalary.getFormatted();
        }
    };

    private static final double AVERAGE_SALARY = 200_000;
    private static final double MIN_SALARY_MULTIPLIER = 0.1;
    private static double salary;
//    private final double employeeSalary;



    public static SalaryType getFormattedTaxesSalary(double salary) {
        setSalary(salary);
        return detector(salary);
    }

    private static SalaryType detector(double salary) {
        if (salary < (AVERAGE_SALARY * MIN_SALARY_MULTIPLIER)) {
            return SALARY_LESS_THAN_10_PROCENT_AVERAGE_SALARY;
        } else if (salary < AVERAGE_SALARY) {
            return SALARY_LESS_THAN_AVERAGE_SALARY;
        } else if (salary > AVERAGE_SALARY) {
            return SALARY_MORE_THAN_AVERAGE_SALARY;
        }
        throw new RuntimeException("Salary type not found");
    }

    public static void setSalary(double salary) {
        SalaryType.salary = salary;
    }

    public abstract String calculateTaxes();
}
