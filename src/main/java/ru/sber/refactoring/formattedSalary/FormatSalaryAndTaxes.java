package ru.sber.refactoring.formattedSalary;

import ru.sber.refactoring.formattedSalary.salaryType.SalaryType;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * метод getResult обрабатывает результат запроса в БД, по итогам которого строка formattedSalary
 * вставляется в форму HTML
 * так же метод возвращает double
 */
public class FormatSalaryAndTaxes {
    public double getResult(ResultSet results, StringBuilder resultingHtml) throws SQLException {
        double result = 0;
        while (results.next()) {
            //format salary and taxes
            double salary = results.getDouble("salary");
            String formattedSalary = SalaryType.getFormattedTaxesSalary(salary).calculateTaxes();
            // process each row of query results
            resultingHtml.append("<tr>"); // add row start tag
            resultingHtml.append("<td>").append(results.getString("emp_name")).append("</td>"); //appending employee name
            resultingHtml.append("<td>").append(formattedSalary).append("</td>"); //appending employee salary for period
            resultingHtml.append("</tr>"); // add row end tag
            result += results.getDouble("salary"); // add salary to totals
        }
        return result;
    }
}
